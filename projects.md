# Projects


## [befreshveg.com](http://befreshveg.com) (Laravel)
```
Befreshveg.com
```
e-commerce multi vendor website build using Laravel framework containing features such as 
* Session based Cart
* User accounts
* Admin account
* Payment gateways (COD,CCAvenue,PayPal)
* User Invoice
* Sale Price, Regular price
* product galllary
* owl coursols
* add to cart and quick view
* bootstrap 3
* html 5
* css 3

## [satyamarg.in](https://satyamarg.in) (Laravel)
```
satyamarg.in
```
blogging website build using laravel framework php 7.1
* User accounts
* Admin and auther accounts
* social sharing
* post commenting 
* comment replys
* One click theme changing
* html 5
* css 3

## [ginie.com](https://ginie.com) (Laravel)
```
ginie.com
```
Inventory mangment 
* Products
* Dynamic products attributes 
* dynamic product 
* Admin accounts
* bootstrap 4 and ionic 
* html 5
* css 3

## [nyaaybhoomi.org](http://nyayabhoomi.org) (Laravel)
```
nyaaybhoomi.org
```
blogging website build using laravel framework php 7.1 
* bootstrap 4
* html 5
* css 3

## [cubagoa.com](http://cubagoa.com) (Codeigniter)
```
cubagoa.com
```
hotel mangment website for multiple hotels build useing codeigniter
* user accounts
* html 5
* css 3

## [happybestwishes.xyz](http://happybestwishes.xyz) (Codeigniter)
```
happybestwishes.xyz
```
online wish sharing website build useing codeigniter
* bootstrap 4
* html 5
* css 3

## [omoads.com](#) (Django)
```
omoads.com
```
online adveriment for offline hoadings build using python Django 
* html 5
* css 3

## [agnisys.com/portal1](http://agnisys.com/portal1/) (Laravel)
```
agnisys.com/portal1
```
Licences and issue mangment for multi company system
* bootstrap 4
* html 5
* css 3

## [shreekant.com](http://shreekantsomany.com) (Laravel)
```
shreekant.com
```
Photography website or protfolio website
* html 5
* css 3

## [articlesadda.com](http://articlesadda.com) (Wordpress)
```
articlesadda.com
```
My own
wordpress blog for latest news


## [frapwise.com](http://frapwise.com) (wordpress)
```
frapwise.com
```
My own 
under constructions

## [localgrocery.in](http://localgrocery.in/) (Laravel)
```
localgrocery.in
```
e-comerce website 
* bootstrap 3
* html 5
* css 3

## [phodeo.in](http://phodeo.in) (Laravel)
```
phodeo.in
```
Photography and events mangment
* bootstrap 4
* html 5
* css 3




