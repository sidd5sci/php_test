




// Minimum number of characters to be replaced to make a given string Palindrome
// ====================================================================================
// Given a string str, the task is to find the minimum number of characters to be replaced to make a given string palindrome. Replacing a character means replacing it with any single character at the same position. We are not allowed to remove or add any characters.

// If there are multiple answers, print the lexicographically smallest string.

// Examples:

// Input: str = "geeks"
// Output: 2
// geeks can be converted to geeeg to make it palindrome
// by replacing minimum characters.

// Input: str = "ameba"
// Output: 1
// We can get "abeba" or "amema" with only 1 change. 
// Among those two, "abeba" is lexicographically smallest.



#include <stdio.h>
// Function to find the minimum number 
// character change required 

void change(char  s[],int n) 
{ 
	// To store the number of replacement operations 
	int cc = 0; 

	for(int i=0;i<n/2;i++) 
	{ 

		// If the characters at location 
		// i and n-i-1 are same then 
		// no change is required 
		if(s[i]== s[n-i-1]) 
			continue; 

		// Counting one change operation 
		cc+= 1; 

		// Changing the character with higher 
		// ascii value with lower ascii value 
		if(s[i]<s[n-i-1]) 
			s[n-i-1]= s[i] ; 
		else
			s[i]= s[n-i-1] ; 
	} 
	printf("Minimum characters to be replaced = %d\n", (cc)) ; 
	
} 
// Driver code 
int main() 
{ 
char s[30] = "geeks"; 

int n = 5;
change(s,n); 
return 0; 
} 
