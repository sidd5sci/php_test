// Recursive program to print all subsets with given sum
// ===========================================================
// Given an array and a number, print all subsets with sum equal to given the sum.

// Examples:

// Input :  arr[] =  {2, 5, 8, 4, 6, 11}, sum = 13
// Output : 
// 5 8
// 2 11
// 2 5 6

// Input : arr[] =  {1, 5, 8, 4, 6, 11}, sum = 9
// Output :
// 5 4
// 1 8



// CPP program to print all subsets with given sum 
#include <stdio.h> 
#include <stdlib.h> 


// A linked list node 
struct Node { 
	int data; 
	struct Node* next; 
}; 
// This function prints contents of linked list starting from 
// the given node 
void printList(struct Node* n) 
{ 
    while (n != NULL) { 
        printf("%d ", n->data); 
        n = n->next; 
    } 
} 
void append(struct Node** head_ref, int new_data) 
{ 
    /* 1. allocate node */
    struct Node* new_node = (struct Node*) malloc(sizeof(struct Node)); 
    struct Node *last = *head_ref;  /* used in step 5*/
    /* 2. put in the data  */
    new_node->data  = new_data; 
    /* 3. This new node is going to be the last node, so make next  
          of it as NULL*/
    new_node->next = NULL; 
    /* 4. If the Linked List is empty, then make the new node as head */
    if (*head_ref == NULL) 
    { 
       *head_ref = new_node; 
       return; 
    }   
    /* 5. Else traverse till the last node */
    while (last->next != NULL) 
        last = last->next; 
   
    /* 6. Change the next of last node */
    last->next = new_node; 
    return;     
} 
// The vector v stores current subset. 
void printAllSubsetsRec(int arr[], int n,struct Node* node,int sum) 
{ 
	// If remaining sum is 0, then print all 
	// elements of current subset. 
	if (sum == 0) { 
        printList(node);
// 		cout << endl; 
		return; 
	} 

	// If no remaining elements, 
	if (n == 0) 
		return; 

	// We consider two cases for every element. 
	// a) We do not include last element. 
	// b) We include last element in current subset. 
	printAllSubsetsRec(arr, n - 1, node, sum); 
    append(&node,arr[n - 1]);
	printAllSubsetsRec(arr, n - 1, node, sum - arr[n - 1]); 
} 

// Wrapper over printAllSubsetsRec() 
void printAllSubsets(int arr[], int n, int sum) 
{ 
  	
  	struct Node* node = (struct Node*)malloc(sizeof(struct Node)); 
	node->data = arr[n - 1];
	node->next = NULL; 
	
	printAllSubsetsRec(arr, n, node,sum); 
} 

// Driver code 
int main() 
{ 
	int arr[] = { 2, 5, 8, 4, 6, 11 }; 
	int sum = 13; 
	int n = sizeof(arr) / sizeof(arr[0]); 
	printAllSubsets(arr, n, sum); 
	return 0; 
} 
